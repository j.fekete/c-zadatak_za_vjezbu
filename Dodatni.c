#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <conio.h>
#define JMBG_LEN 13



typedef struct datum{
    int dan;
    int mjesec;
    int godina;
    struct datum *next;
} datum_t;

void bubbleSort(datum_t *head); 

void swap(datum_t *a, datum_t *b); 

datum_t *fdatum(char *jmbg){
    char tmp[JMBG_LEN];
    
    datum_t *result = malloc(sizeof(datum_t));
    snprintf(tmp,3,"%s",&jmbg[0]);
    //printf("%s\n",tmp);
    result->dan = atoi(tmp);
    //printf("%d\n",result->dan);
    snprintf(tmp,3,"%s",&jmbg[2]);
    //printf("%s\n",tmp);
    result->mjesec=atoi(tmp);
    //printf("%d\n",result->mjesec);
    snprintf(tmp,5,"%s",&jmbg[4]);
    //printf("%s\n",tmp);
    result->godina=atoi(tmp);
    //printf("%d\n",result->godina);
    return result;
}

void printlist(datum_t *head){
    datum_t *tmp = head;
    
    while (tmp)
    {
        printf("<<-- %d.%d.%d ",tmp->dan,tmp->mjesec,tmp->godina);
        tmp = tmp->next;
    }
    }

void cleanInput(char* input) {
	int len = strlen(input)-1;
	input[len] = '\0';
}

void bubbleSort(datum_t *head) 
{ 
    int swapped, i; 
    datum_t *ptr1; 
    datum_t *lptr = NULL; 
  
    if (head == NULL) 
        return; 
  
    do
    { 
        swapped = 0; 
        ptr1 = head; 
  
        while (ptr1->next != lptr) 
        { 
            if (ptr1->godina > ptr1->next->godina) 
            {  
                swap(ptr1, ptr1->next); 
                swapped = 1; 
            } else if (ptr1->godina == ptr1->next->godina)
            {
                if (ptr1->mjesec > ptr1->next->mjesec)
                {
                    swap(ptr1, ptr1->next); 
                swapped = 1;
                }
                else if (ptr1->mjesec == ptr1->next->mjesec)
                {
                if (ptr1->dan > ptr1->next->dan)
                {
                    swap(ptr1, ptr1->next); 
                swapped = 1;
                }

            }
            }
            ptr1 = ptr1->next; 
        } 
        lptr = ptr1; 
    } 
    while (swapped); 
} 

void bubbleSort2(datum_t *head) 
{ 
    int swapped, i; 
    datum_t *ptr1; 
    datum_t *lptr = NULL; 
  
    if (head == NULL) 
        return; 
  
    do
    { 
        swapped = 0; 
        ptr1 = head; 
  
        while (ptr1->next != lptr) 
        { 
            if (ptr1->godina < ptr1->next->godina) 
            {  
                swap(ptr1, ptr1->next); 
                swapped = 1; 
            } else if (ptr1->godina == ptr1->next->godina)
            {
                if (ptr1->mjesec < ptr1->next->mjesec)
                {
                    swap(ptr1, ptr1->next); 
                swapped = 1;
                }
                else if (ptr1->mjesec == ptr1->next->mjesec)
                {
                if (ptr1->dan < ptr1->next->dan)
                {
                    swap(ptr1, ptr1->next); 
                swapped = 1;
                }

            }
            }
            ptr1 = ptr1->next; 
        } 
        lptr = ptr1; 
    } 
    while (swapped); 
} 

void swap(datum_t *a, datum_t *b) 
{ 
    int temp = a->dan; 
    a->dan = b->dan; 
    b->dan = temp; 
    temp = a->mjesec; 
    a->mjesec = b->mjesec; 
    b->mjesec = temp; 
    temp = a->godina; 
    a->godina = b->godina; 
    b->godina = temp; 
} 


int main()
{
    char *jmbg;
    datum_t *head = NULL;
    datum_t *tmp;
    printf("\n\n Unesite JMBG: \n");
    scanf("%s",jmbg);
    do
    {       
        tmp = fdatum(jmbg);
        tmp->next = head;
        head = tmp; 
        memset(jmbg, 0, strlen(jmbg));
        printf("\n\n Unesite JMBG: \n");
        scanf("%s",jmbg);

        }while (strcmp(jmbg,"!") == 1);

    printlist(head);
    printf("\n\n");
    bubbleSort(head);
    printlist(head);
    printf("\n\n");
    bubbleSort2(head);
    printlist(head);
    getch();
    return 0;
}
