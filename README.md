## C - ZADATAK ZA VJEZBU<br>
Napisati funkciju koja kao argument prihvata string JMBG (jedinstveni maticni broj gradjanina), <br> a kao izlaz vraca strukturnu varijablu koja 
predstavlja datum rodjenja doticne osobe.<br> <br> 
Prototip funkcije je:
struct datum fdatum(char *jmbg)
gde struct datum predstavlja strukturu definisanu kao:
struct datum
{
int dan;
int mjesec;
int godina;
};<br>
Na primer, ako je strukturna varijabla drodj definisana kao:
struct datum drodj; te ako je ulaz u funkciju jmbg=“2405978234567“ poziv funkcije drodj=fdatum(jmbg); treba vratiti strukturnu varijablu koja predstavlja datum 24.05.1978., tj. funkcija vraca sledece podatke strukturne varijable tipa datum:
drodj.dan=24;
drodj.mjesec=5;
drodj.godina=1978;<br> <br> 
Nakon toga napišite glavni program koji sa standardnog ulaza ucitava JMBG kao znakovno polje, nakon toga poziva funkciju fdatum(), ispisuje datum rodjenja osobe sa doticnim jedinstvenim maticnim brojem, i smesta novi datum u jednostruko spregnutu listu.<br> <br> 
Realizovati f-ju za sortiranje liste na osnovu datuma (od starijeg datuma ka novijem).<br> <br> 
Realizovati i funkciju za ispis svih unetih datuma iz jednostruko spregnute liste (u redovnom i obrnutom redosledu).
