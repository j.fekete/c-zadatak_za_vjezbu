	.file	"Dodatni.c"
	.section .rdata,"dr"
LC0:
	.ascii "%s\0"
	.text
	.globl	_fdatum
	.def	_fdatum;	.scl	2;	.type	32;	.endef
_fdatum:
LFB17:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$56, %esp
	movl	$16, (%esp)
	call	_malloc
	movl	%eax, -12(%ebp)
	movl	8(%ebp), %eax
	movl	%eax, 12(%esp)
	movl	$LC0, 8(%esp)
	movl	$3, 4(%esp)
	leal	-25(%ebp), %eax
	movl	%eax, (%esp)
	call	_snprintf
	leal	-25(%ebp), %eax
	movl	%eax, (%esp)
	call	_atoi
	movl	%eax, %edx
	movl	-12(%ebp), %eax
	movl	%edx, (%eax)
	movl	8(%ebp), %eax
	addl	$2, %eax
	movl	%eax, 12(%esp)
	movl	$LC0, 8(%esp)
	movl	$3, 4(%esp)
	leal	-25(%ebp), %eax
	movl	%eax, (%esp)
	call	_snprintf
	leal	-25(%ebp), %eax
	movl	%eax, (%esp)
	call	_atoi
	movl	%eax, %edx
	movl	-12(%ebp), %eax
	movl	%edx, 4(%eax)
	movl	8(%ebp), %eax
	addl	$4, %eax
	movl	%eax, 12(%esp)
	movl	$LC0, 8(%esp)
	movl	$5, 4(%esp)
	leal	-25(%ebp), %eax
	movl	%eax, (%esp)
	call	_snprintf
	leal	-25(%ebp), %eax
	movl	%eax, (%esp)
	call	_atoi
	movl	%eax, %edx
	movl	-12(%ebp), %eax
	movl	%edx, 8(%eax)
	movl	-12(%ebp), %eax
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE17:
	.section .rdata,"dr"
LC1:
	.ascii "<<-- %d.%d.%d \0"
	.text
	.globl	_printlist
	.def	_printlist;	.scl	2;	.type	32;	.endef
_printlist:
LFB18:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$40, %esp
	movl	8(%ebp), %eax
	movl	%eax, -12(%ebp)
	jmp	L4
L5:
	movl	-12(%ebp), %eax
	movl	8(%eax), %ecx
	movl	-12(%ebp), %eax
	movl	4(%eax), %edx
	movl	-12(%ebp), %eax
	movl	(%eax), %eax
	movl	%ecx, 12(%esp)
	movl	%edx, 8(%esp)
	movl	%eax, 4(%esp)
	movl	$LC1, (%esp)
	call	_printf
	movl	-12(%ebp), %eax
	movl	12(%eax), %eax
	movl	%eax, -12(%ebp)
L4:
	cmpl	$0, -12(%ebp)
	jne	L5
	nop
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE18:
	.globl	_cleanInput
	.def	_cleanInput;	.scl	2;	.type	32;	.endef
_cleanInput:
LFB19:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$40, %esp
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_strlen
	subl	$1, %eax
	movl	%eax, -12(%ebp)
	movl	-12(%ebp), %edx
	movl	8(%ebp), %eax
	addl	%edx, %eax
	movb	$0, (%eax)
	nop
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE19:
	.globl	_bubbleSort
	.def	_bubbleSort;	.scl	2;	.type	32;	.endef
_bubbleSort:
LFB20:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$40, %esp
	movl	$0, -20(%ebp)
	cmpl	$0, 8(%ebp)
	je	L15
L8:
	movl	$0, -12(%ebp)
	movl	8(%ebp), %eax
	movl	%eax, -16(%ebp)
	jmp	L10
L14:
	movl	-16(%ebp), %eax
	movl	8(%eax), %edx
	movl	-16(%ebp), %eax
	movl	12(%eax), %eax
	movl	8(%eax), %eax
	cmpl	%eax, %edx
	jle	L11
	movl	-16(%ebp), %eax
	movl	12(%eax), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_swap
	movl	$1, -12(%ebp)
	jmp	L12
L11:
	movl	-16(%ebp), %eax
	movl	8(%eax), %edx
	movl	-16(%ebp), %eax
	movl	12(%eax), %eax
	movl	8(%eax), %eax
	cmpl	%eax, %edx
	jne	L12
	movl	-16(%ebp), %eax
	movl	4(%eax), %edx
	movl	-16(%ebp), %eax
	movl	12(%eax), %eax
	movl	4(%eax), %eax
	cmpl	%eax, %edx
	jle	L13
	movl	-16(%ebp), %eax
	movl	12(%eax), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_swap
	movl	$1, -12(%ebp)
	jmp	L12
L13:
	movl	-16(%ebp), %eax
	movl	4(%eax), %edx
	movl	-16(%ebp), %eax
	movl	12(%eax), %eax
	movl	4(%eax), %eax
	cmpl	%eax, %edx
	jne	L12
	movl	-16(%ebp), %eax
	movl	(%eax), %edx
	movl	-16(%ebp), %eax
	movl	12(%eax), %eax
	movl	(%eax), %eax
	cmpl	%eax, %edx
	jle	L12
	movl	-16(%ebp), %eax
	movl	12(%eax), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_swap
	movl	$1, -12(%ebp)
L12:
	movl	-16(%ebp), %eax
	movl	12(%eax), %eax
	movl	%eax, -16(%ebp)
L10:
	movl	-16(%ebp), %eax
	movl	12(%eax), %eax
	cmpl	-20(%ebp), %eax
	jne	L14
	movl	-16(%ebp), %eax
	movl	%eax, -20(%ebp)
	cmpl	$0, -12(%ebp)
	jne	L8
	jmp	L7
L15:
	nop
L7:
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE20:
	.globl	_bubbleSort2
	.def	_bubbleSort2;	.scl	2;	.type	32;	.endef
_bubbleSort2:
LFB21:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$40, %esp
	movl	$0, -20(%ebp)
	cmpl	$0, 8(%ebp)
	je	L24
L17:
	movl	$0, -12(%ebp)
	movl	8(%ebp), %eax
	movl	%eax, -16(%ebp)
	jmp	L19
L23:
	movl	-16(%ebp), %eax
	movl	8(%eax), %edx
	movl	-16(%ebp), %eax
	movl	12(%eax), %eax
	movl	8(%eax), %eax
	cmpl	%eax, %edx
	jge	L20
	movl	-16(%ebp), %eax
	movl	12(%eax), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_swap
	movl	$1, -12(%ebp)
	jmp	L21
L20:
	movl	-16(%ebp), %eax
	movl	8(%eax), %edx
	movl	-16(%ebp), %eax
	movl	12(%eax), %eax
	movl	8(%eax), %eax
	cmpl	%eax, %edx
	jne	L21
	movl	-16(%ebp), %eax
	movl	4(%eax), %edx
	movl	-16(%ebp), %eax
	movl	12(%eax), %eax
	movl	4(%eax), %eax
	cmpl	%eax, %edx
	jge	L22
	movl	-16(%ebp), %eax
	movl	12(%eax), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_swap
	movl	$1, -12(%ebp)
	jmp	L21
L22:
	movl	-16(%ebp), %eax
	movl	4(%eax), %edx
	movl	-16(%ebp), %eax
	movl	12(%eax), %eax
	movl	4(%eax), %eax
	cmpl	%eax, %edx
	jne	L21
	movl	-16(%ebp), %eax
	movl	(%eax), %edx
	movl	-16(%ebp), %eax
	movl	12(%eax), %eax
	movl	(%eax), %eax
	cmpl	%eax, %edx
	jge	L21
	movl	-16(%ebp), %eax
	movl	12(%eax), %eax
	movl	%eax, 4(%esp)
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_swap
	movl	$1, -12(%ebp)
L21:
	movl	-16(%ebp), %eax
	movl	12(%eax), %eax
	movl	%eax, -16(%ebp)
L19:
	movl	-16(%ebp), %eax
	movl	12(%eax), %eax
	cmpl	-20(%ebp), %eax
	jne	L23
	movl	-16(%ebp), %eax
	movl	%eax, -20(%ebp)
	cmpl	$0, -12(%ebp)
	jne	L17
	jmp	L16
L24:
	nop
L16:
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE21:
	.globl	_swap
	.def	_swap;	.scl	2;	.type	32;	.endef
_swap:
LFB22:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$16, %esp
	movl	8(%ebp), %eax
	movl	(%eax), %eax
	movl	%eax, -4(%ebp)
	movl	12(%ebp), %eax
	movl	(%eax), %edx
	movl	8(%ebp), %eax
	movl	%edx, (%eax)
	movl	12(%ebp), %eax
	movl	-4(%ebp), %edx
	movl	%edx, (%eax)
	movl	8(%ebp), %eax
	movl	4(%eax), %eax
	movl	%eax, -4(%ebp)
	movl	12(%ebp), %eax
	movl	4(%eax), %edx
	movl	8(%ebp), %eax
	movl	%edx, 4(%eax)
	movl	12(%ebp), %eax
	movl	-4(%ebp), %edx
	movl	%edx, 4(%eax)
	movl	8(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -4(%ebp)
	movl	12(%ebp), %eax
	movl	8(%eax), %edx
	movl	8(%ebp), %eax
	movl	%edx, 8(%eax)
	movl	12(%ebp), %eax
	movl	-4(%ebp), %edx
	movl	%edx, 8(%eax)
	nop
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE22:
	.def	___main;	.scl	2;	.type	32;	.endef
	.section .rdata,"dr"
LC2:
	.ascii "\12\12 Unesite JMBG: \0"
LC3:
	.ascii "!\0"
LC4:
	.ascii "\12\0"
	.text
	.globl	_main
	.def	_main;	.scl	2;	.type	32;	.endef
_main:
LFB23:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	andl	$-16, %esp
	subl	$32, %esp
	call	___main
	movl	$0, 28(%esp)
	movl	$LC2, (%esp)
	call	_puts
	movl	24(%esp), %eax
	movl	%eax, 4(%esp)
	movl	$LC0, (%esp)
	call	_scanf
L27:
	movl	24(%esp), %eax
	movl	%eax, (%esp)
	call	_fdatum
	movl	%eax, 20(%esp)
	movl	20(%esp), %eax
	movl	28(%esp), %edx
	movl	%edx, 12(%eax)
	movl	20(%esp), %eax
	movl	%eax, 28(%esp)
	movl	24(%esp), %eax
	movl	%eax, (%esp)
	call	_strlen
	movl	%eax, 8(%esp)
	movl	$0, 4(%esp)
	movl	24(%esp), %eax
	movl	%eax, (%esp)
	call	_memset
	movl	$LC2, (%esp)
	call	_puts
	movl	24(%esp), %eax
	movl	%eax, 4(%esp)
	movl	$LC0, (%esp)
	call	_scanf
	movl	$LC3, 4(%esp)
	movl	24(%esp), %eax
	movl	%eax, (%esp)
	call	_strcmp
	cmpl	$1, %eax
	je	L27
	movl	28(%esp), %eax
	movl	%eax, (%esp)
	call	_printlist
	movl	$LC4, (%esp)
	call	_puts
	movl	28(%esp), %eax
	movl	%eax, (%esp)
	call	_bubbleSort
	movl	28(%esp), %eax
	movl	%eax, (%esp)
	call	_printlist
	movl	$LC4, (%esp)
	call	_puts
	movl	28(%esp), %eax
	movl	%eax, (%esp)
	call	_bubbleSort2
	movl	28(%esp), %eax
	movl	%eax, (%esp)
	call	_printlist
	call	_getch
	movl	$0, %eax
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE23:
	.ident	"GCC: (MinGW.org GCC-6.3.0-1) 6.3.0"
	.def	_malloc;	.scl	2;	.type	32;	.endef
	.def	_snprintf;	.scl	2;	.type	32;	.endef
	.def	_atoi;	.scl	2;	.type	32;	.endef
	.def	_printf;	.scl	2;	.type	32;	.endef
	.def	_strlen;	.scl	2;	.type	32;	.endef
	.def	_puts;	.scl	2;	.type	32;	.endef
	.def	_scanf;	.scl	2;	.type	32;	.endef
	.def	_memset;	.scl	2;	.type	32;	.endef
	.def	_strcmp;	.scl	2;	.type	32;	.endef
	.def	_getch;	.scl	2;	.type	32;	.endef
